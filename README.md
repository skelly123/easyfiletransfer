# Monitored File Transfer 

This programme will allow you to move a folder from one place to another
including all the files contained within it. Not only move, but also monitor the progress and create opportunities for users to edit the file if a transfer error
is fixable. 

File transfer can take a long time, so if you do not answer within 10 seconds 
the programme moves onto the next file and the files can be reviewed at the end

## How to

1. Download this code.
2. Go into the folder containing this code in your terminal.
3. Install pipenv for python3 
4. Install the virtualenv by running `pipenv install`
5. Enter the virtualenv `pipenv shell`
6. Run the programme by entering 
- `python main.py -s <the path for your files to copy> -d <the path for where you want the files to go>`.

7. Answer the questions in the terminal.
8. Wait.