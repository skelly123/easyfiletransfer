import sqlite3
import os
from os.path import join, getsize
import time

DB_NAME = 'files.db'

class FileDB:
    def __init__(self, source_folder):
        self.connection = self.get_db()
         # if database has been populated
        print(f"filled: {self.filled}")
        if not self.filled:
            self.fill(source_folder)
        self.state = self.get_state()

    def get_db(self):
        """
        Gets a DB connection for user
        """
        db = self.check_if_db_exists()
        self.filled = False
        if db:
            accepted_response = False
            while not accepted_response:
                carry_on_response = input("Do you want to carry on from previous attempt? [y/n]: ")
                carry_on_response = carry_on_response.lower()
                if (carry_on_response not in ['y', 'n']):
                    print("Invalid input type 'y' or 'n'")
                else:
                    accepted_response = True
            if carry_on_response == 'y':
                self.filled = True
                print("Using previous files database for copying")
            else:
                self.remove_old_db(db)
                db = self.create_db()
        else:
            print("no db")
            db = self.create_db()
        return sqlite3.connect(db)

    def check_if_db_exists(self):
        """
        Checks main folder for a DB file and returns db file path
        """
        db = None
        dir_path = os.path.dirname(os.path.realpath(__file__))
        files_in_dir = os.listdir(dir_path)
        if DB_NAME in files_in_dir:
            db = dir_path + DB_NAME
        return db

    def create_db(self):
        """
        Create a DB in the main folder
        """
        print("Creating a new files DB from your folder")
        dir_path = os.path.dirname(os.path.realpath(__file__))
        try:
            os.utime(DB_NAME, None)
        except OSError:
            print("HI")
            open(DB_NAME, 'a').close()
        return dir_path + DB_NAME

    def remove_old_db(self, old_db):
        os.remove(old_db)
        time.sleep(0.1)
        return

    def fill(self, folder):
        # TODO: Get list of files
        # create table
        c = self.connection.cursor()
        c.execute('''
        CREATE TABLE files
        (filepath, copied, error, resolved)
        ''')

        file_list = [ (os.path.join(d, x).replace( folder, ''), False, '', False) for d, dirs, files in os.walk(folder) for x in files ]
        c.executemany('INSERT INTO files VALUES (?,?,?,?)', file_list)
        return

    def count_unattempted(self):
        # make a db query
        c = self.connection.cursor()
        # count files left
        c.execute('''
        SELECT COUNT(*) FROM files WHERE copied=False AND error=''
        ''')
        count = c.fetchone()[0]
        return count
    
    def get_unattempted_file(self):
        # make a db query
        c = self.connection.cursor()
        # count files left
        c.execute('''
        SELECT * FROM files WHERE copied=False AND error=''
        ''')
        count = c.fetchone()[0]
        return count

    def count_unresolved_errors(self):
        # make a db query
        c = self.connection.cursor()
        # count files left
        c.execute('''
        SELECT COUNT(*) FROM files WHERE error != '' AND resolved=False
        ''')
        count = c.fetchone()[0]
        return count
    
    def get_unresolved_error_file(self):
        # make a db query
        c = self.connection.cursor()
        # count files left
        c.execute('''
        SELECT * FROM files WHERE error != '' AND resolved=False
        ''')
        count = c.fetchone()[0]
        return count

    def count_transfered(self):
        # make a db query
        c = self.connection.cursor()
        # count files left
        c.execute('''
        SELECT COUNT(*) FROM files WHERE copied=True
        ''')
        count = c.fetchone()[0]
        return count

    def get_next_file(self, error=False):
        c = self.connection.cursor()
        if error:
            c.execute('''
            SELECT * FROM files WHERE copied=False AND error!='' AND resolved=False LIMIT 1 
            ''')
        else:
            c.execute('''
            SELECT * FROM files WHERE copied=False AND error='' LIMIT 1
            ''')
        file = c.fetchone()[0]
        return file

    def count_files_left_to_process(self):
        count = 0
        count += self.count_unattempted()
        count += self.count_unresolved_errors()
        return count

    def set_file_error(self, filepath, error):
        c = self.connection.cursor()
        c.execute('''
        UPDATE files SET error = ? WHERE filepath = ?
        ''', (str(error), filepath))
        return

    def set_file_error_resolved(self, filepath):
        c = self.connection.cursor()
        c.execute('''
        UPDATE files SET resolved = ? WHERE filepath = ?
        ''', (True, filepath))
        return

    def set_file_transfer_successful(self, filepath):
        c = self.connection.cursor()
        c.execute('''
        UPDATE files SET copied = ? WHERE filepath = ?
        ''', (True, filepath))
        return

    def get_state(self):
        num_of_unattempted  = self.count_unattempted()
        if num_of_unattempted > 0:
            state = {
                'name': 'UNATTEMPTED',
                'getter': self.get_unattempted_file,
                'count': num_of_unattempted
            }
        else:
            num_of_errored_and_unresolved = self.count_unresolved_errors()
            if num_of_errored_and_unresolved > 0:
                state = {
                    'name': 'ERRORED',
                    'getter': self.get_errored_unresolved_file,
                    'count': num_of_errored_and_unresolved
                }
            else:
                state = {
                    'name': 'FINISHED',
                    'getter': lambda : None,
                    'count': 0
                }
        return state

if __name__ == "__main__":
    db = FileDB()
    print(db.connection)
