"""
This programme will move files from one place to another on your system.
This is specifically aimed at moving the contents of one hard drive to another.

Aims:
* Move all files from one place to another
* If a file does not get copied it will recover
  * pause and tell the user what happened
  * give the user an option of what to do
    * give them the opportunity to change something about the file and retry moving it
    * skip the file and others with similar reasons
* It should show progress 
* If stopped we should be able to restart from the same position
"""

import shutil
import argparse
from file_db import FileDB
from tqdm import tqdm
import os


parser = argparse.ArgumentParser(description='Move files.')
parser.add_argument("-s", "--source", required=True, help='The folder you want to copy files from')
parser.add_argument("-d", "--destination", required=True, help='The folder you want to copy files to')
args = parser.parse_args()

SOURCE_FOLDER = args.source
DESTINATION_FOLDER = args.destination

def attempt_transfer(db, source_folder, destination_folder, filepath):
    old_path = f"{source_folder}{filepath}"
    new_path = f"{destination_folder}{filepath}"
    os.makedirs(os.path.dirname(new_path), exist_ok=True)
    shutil.copy(old_path, new_path)
    db.set_file_transfer_successful(filepath)
    return

def process_error(filepath, source_folder, destination_folder):
    accepted_response = False
    while not accepted_response:
        print(f"The file {filepath} did not succesfully transfer.")
        retry_or_skip = input("Do you want to 1. change something and retry transfer or 2. skip file: ")
        if retry_or_skip in ['1', '2']:
            accepted_response = True
        else:
            print("Choose 1 or 2.")
        if retry_or_skip == '1': # retry
            try:
                attempt_transfer(db, source_folder, destination_folder, filepath)
            except Exception as e:
                db.set_file_error_resolved(filepath)
                # TO DO: ask user what they want to do now 
                continue
        else: # skip
            # update db with error
            db.set_file_error_resolved(filepath)
            continue   
    return


def main(source_folder, destination_folder):
    print("WELCOME TO THE FILE TRANSFER PROGRAMME!")
    print(f"This programme will transfer the files from {source_folder} to {destination_folder}")

    # connect to/create database
    db = FileDB(source_folder)

    # do transfer
    print("Beginning transfer")

    files_left_to_process = db.count_files_left_to_process()
    progress_bar =  tqdm(range(files_left_to_process)) # need query to be how many are unsuccesfully copied atm without resolved errors
    state = db.get_state() 
   
    while state['name'] != 'FINISHED':
        print(state['name'].upper())
        for _ in range(state['count']):
            file_getter = state['getter']
            file = file_getter()
            print(file)
            processed = False
            while not processed:
                try:
                    attempt_transfer(db, source_folder, destination_folder, file)
                    processed = True
                except Exception as e:
                    # if error process it
                    if state['name'] == 'ERRORED':
                        process_error(e, source_folder, destination_folder)
                        processed = True
                    else:
                        db.set_file_error(file, e)
                        processed = True

            progress_bar.update()
        state = db.get_state()

    print("Successfully copied all files. Now exiting")


if __name__ == "__main__":
    main(SOURCE_FOLDER, DESTINATION_FOLDER)

